import React, {Component} from 'react';
import QrReader from 'react-qr-reader'
import './App.css';

class App extends Component {

    state = {
        isCameraDisable: false,
    };

    handleScan = data => {
        if (!window.opener) {
            alert("画面が正しく開かれませんでした。");
            window.open('', '_self').close();
            return;
        }
        if (data) {
            window.opener.onQrScan(data);
            window.open('', '_self').close();
        } else if (this.state.isCameraDisable) {
            alert("写真からQRコードを認識できませんでした。恐れ入りますが、QRコードが画面中央にはっきり映るように撮影してください。");
        }
    };

    handleError = err => {
        alert("「QRコードを選択」からQRコードの写真を撮影してください。");
        this.setState({isCameraDisable: true});
    };

    openImageDialog = () => {
        this.refs.qrReader.openImageDialog();
    };

    render() {
        return (
            <div>
                <QrReader
                    ref="qrReader"
                    delay={500}
                    onError={this.handleError}
                    onScan={this.handleScan}
                    style={{width: '100%'}}
                    legacyMode={this.state.isCameraDisable}
                />
                {(() => {
                    if (this.state.isCameraDisable) {
                        return <input
                            type="button"
                            value="QRコードを選択"
                            onClick={this.openImageDialog}/>
                    }
                })()}
            </div>
        )
    }
}

export default App;
